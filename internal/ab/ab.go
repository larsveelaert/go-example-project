package ab

func A() {
	b("test")
}

//If a function in that packagee should be mocked (prefer to pull it in another pkg)
//Use a variable function to replace the function with a mock in testing
var b = func(test string) bool {
	return true
}
