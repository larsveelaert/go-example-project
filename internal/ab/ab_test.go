package ab

import (
	//"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"testing"
)

//TODO TODO TODO store the real functions first??? Keepin it real?, thats not professional?

type Mock struct {
	mock.Mock
}

func (m *Mock) bMock(arg string) bool {
	args := m.Called(arg)
	return args.Bool(0)
}

func TestA(t *testing.T) {
	m := new(Mock)

	bOrg := b
	b = m.bMock
	defer func() { b = bOrg }()

	m.On("bMock", "test").Return(true)
	A()
	m.AssertExpectations(t)
}
