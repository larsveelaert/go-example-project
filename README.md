# go-example-project

## General
- Use go.mod for defenition of base package
- Make sure the default go test, build, run, work in the root of the package

## Code / Structure
- Overpackage with structure by working under internal/ folder
- Functions have 1 responsability, keep it short, but don't make it too complex
- Dependencies on procedures that make the test cases branch out, should be in other packages

## Testing
- Testify & Mock package examples can be found in repo
- Use table tests as default when many params, just so you can copy paste
- Test all exported functions of the package, if not exported but very complex and with lots of dependencies, use the variable method with a defer -> very exceptional to also mock something that you will test in the same file
- Test first, even just so you don't polute main()
- Tests should never use other functions, defenitly not in other files

? How use interfaces for mocking?

- 

